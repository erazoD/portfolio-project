import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Buefy from 'buefy'
//import 'buefy/dist/buefy.css'

Vue.config.productionTip = false
Vue.use(Buefy)
Vue.use(VueRouter)

import router from './routes.js'


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
