import VueRouter from 'vue-router';
import Home from './components/pages/Home.vue';
import Showcase from './components/project/Showcase.vue';

let routes = [
  {path:'/', name:'', component:Home},
  {path:'/project/:id', name:'project' ,component:Showcase}
]

export default new VueRouter({
  mode: 'history',
  hash: false,
  routes});
